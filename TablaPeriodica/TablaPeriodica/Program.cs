﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TablaPeriodica
{
    class Program
    {
        static void Main(string[] args)
        {
            byte opcionMenu;
            byte siglo;
            const int PosicionX=50;

            //se crea la colección para almacenar los elementos y se llama al método cargar para añadir los elementos
            //almacenados en el fichero
            ArrayList listadoElementos = new ArrayList();
            listadoElementos=GestorElementos.Cargar();

            Tabla.MostrarSigloActual(listadoElementos);

            do
            {
                Console.SetCursorPosition(PosicionX, 1);
                Console.WriteLine("**Menú de opciones**");
                Console.SetCursorPosition(PosicionX, 2);
                Console.WriteLine("1. Ver todos los siglos.");
                Console.SetCursorPosition(PosicionX, 3);
                Console.WriteLine("2. Ver siglo.");
                Console.SetCursorPosition(PosicionX, 4);
                Console.WriteLine("3. Nuevo Elemento.");
                Console.SetCursorPosition(PosicionX, 5);
                Console.WriteLine("4. Salir.");
                Console.SetCursorPosition(PosicionX, 6);
                Console.WriteLine("Elije una opción:");
                Console.SetCursorPosition(PosicionX, 7);
                opcionMenu = Convert.ToByte(Console.ReadLine());
            
                switch (opcionMenu)
                {
                    case 1: Tabla.MostrarTodosSiglos(listadoElementos); break;
                    case 2:
                    {
                            Console.SetCursorPosition(PosicionX, 8);
                        Console.WriteLine("**Ver Siglo**");
                        Console.SetCursorPosition(PosicionX, 9);
                        Console.WriteLine("Indique el siglo que desea mostrar (0 a 21):");
                        Console.SetCursorPosition(PosicionX, 10);
                        siglo = Convert.ToByte(Console.ReadLine());
                        Console.Clear();
                        Tabla.MostrarSiglo(listadoElementos, siglo); break;
                    }                        
                    case 3: NuevoElemento(listadoElementos); break;
                    case 4: GestorElementos.Guardar(listadoElementos); break;
                    default: Console.WriteLine("Opción no válida"); break;
                }
            } while (opcionMenu != 4);
        }
        //método que añade un elemento nuevo a la colección de elementos
        private static void NuevoElemento(ArrayList listadoElementos)
        {
            //Variables propias del método
            string tipoElemento;
            string nombre;
            string simboloQuimico;
            byte numeroAtomico;
            string anyoDescubrimiento;
            string liquido;
            bool esLiquido=false;
            string color;
            string metalNoble;
            bool esMetalNoble=false;
            string metalRefractario;
            bool esMetalRefractario = false;
            string gasNoble;
            bool esGasNoble = false;

            Console.Clear();
            Console.WriteLine("**Nuevo Elemento**");
            Console.WriteLine();
            do
            {
                Console.WriteLine("Indique el tipo de elemento que desea añadir:");
                Console.WriteLine("Metal, Metaloide o NoMetal");
                tipoElemento = Console.ReadLine();
            } while (!((tipoElemento.ToUpper().Equals(("metal").ToUpper()))||
                (tipoElemento.ToUpper().Equals(("Metaloide").ToUpper())) ||
                (tipoElemento.ToUpper().Equals(("NoMetal").ToUpper()))));
            Console.WriteLine("Indique el nombre del elemento:");
            nombre = Console.ReadLine();
            Console.WriteLine("Indique el símbolo químico del elemento:");
            simboloQuimico = Console.ReadLine();
            Console.WriteLine("Indique el número atómico del elemento:");
            numeroAtomico = Convert.ToByte(Console.ReadLine());
            Console.WriteLine("Indique el año de descubrimiento del elemento:");
            Console.WriteLine("Para elementos antiguos (Antes de Cristo) introduzca cualquier valor negativo");
            anyoDescubrimiento = Console.ReadLine(); 
            if (tipoElemento.ToUpper().Equals("metal".ToUpper()))
            {
                do
                {
                    Console.WriteLine("¿Es un líquido a temperatura ambiente?(si/no):");
                    liquido = Console.ReadLine();
                } while (!((liquido.ToUpper().Equals(("si").ToUpper())) || 
                        (liquido.ToUpper().Equals(("no").ToUpper()))));
                if (liquido.ToUpper().Equals(("si").ToUpper()))
                {
                    esLiquido = true;
                }
                else if (liquido.ToUpper().Equals(("no").ToUpper()))
                {
                    esLiquido = false;
                }
                Console.WriteLine("Indique el color del elemento:");
                color = Console.ReadLine();
                do
                {
                    Console.WriteLine("¿Es un metal noble?(si/no):");
                    metalNoble = Console.ReadLine();
                } while (!((metalNoble.ToUpper().Equals(("si").ToUpper())) ||
                        (metalNoble.ToUpper().Equals(("no").ToUpper()))));
                if (metalNoble.ToUpper().Equals(("si").ToUpper()))
                {
                    esMetalNoble = true;
                }
                else if (metalNoble.ToUpper().Equals(("no").ToUpper()))
                {
                    esMetalNoble = false;
                }
                listadoElementos.Add(new Metal(nombre, simboloQuimico, numeroAtomico, 
                    anyoDescubrimiento, esLiquido, color, esMetalNoble));
            }
            if (tipoElemento.ToUpper().Equals(("metaloide").ToUpper()))
            {
                do
                {
                    Console.WriteLine("¿Es un metal refractario?(si/no):");
                    metalRefractario = Console.ReadLine();
                } while (!((metalRefractario.ToUpper().Equals(("si").ToUpper())) ||
                        (metalRefractario.ToUpper().Equals(("no").ToUpper()))));
                if (metalRefractario.ToUpper().Equals(("si").ToUpper()))
                {
                    esMetalRefractario = true;
                }
                else if (metalRefractario.ToUpper().Equals(("no").ToUpper()))
                {
                    esMetalRefractario = false;
                }
                do
                {
                    Console.WriteLine("¿Es un metal noble?(si/no):");
                    metalNoble = Console.ReadLine();
                } while (!((metalNoble.ToUpper().Equals(("si").ToUpper())) ||
                        (metalNoble.ToUpper().Equals(("no").ToUpper()))));
                if (metalNoble.ToUpper().Equals(("si").ToUpper()))
                {
                    esMetalNoble = true;
                }
                else if (metalNoble.ToUpper().Equals(("no").ToUpper()))
                {
                    esMetalNoble = false;
                }
                listadoElementos.Add(new Metaloide(nombre, simboloQuimico, numeroAtomico, 
                    anyoDescubrimiento, esMetalRefractario, esMetalNoble));
                if (tipoElemento.ToUpper().Equals(("nometal").ToUpper()))
                {
                    do
                    {
                        Console.WriteLine("¿Es un gas noble?(si/no):");
                        gasNoble = Console.ReadLine();
                    } while (!((gasNoble.ToUpper().Equals(("si").ToUpper())) ||
                            (gasNoble.ToUpper().Equals(("no").ToUpper()))));
                    if (gasNoble.ToUpper().Equals(("si").ToUpper()))
                    {
                        esGasNoble = true;
                    }
                    else if (gasNoble.ToUpper().Equals(("no").ToUpper()))
                    {
                        esGasNoble = false;
                    }
                    listadoElementos.Add(new NoMetal(nombre, simboloQuimico, numeroAtomico, 
                        anyoDescubrimiento, esGasNoble));
                }
            }

        }
    }
}
