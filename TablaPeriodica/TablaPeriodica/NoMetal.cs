﻿/*En esta clase se crean los Elementos del tipo NoMetal
 Contiene atributos, método constructor, métodos de modificación y acceso a los atributos y
 la sobrecarga del método ToString utilizado en la escritura del fichero de texto*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TablaPeriodica
{
    class NoMetal :Elemento
    {
        //atributos
        private bool esGasNoble;
        
        //método constructor
        public NoMetal(string nombre, string simboloQuimico, byte numeroAtomico, 
            string anyoDescubrimeinto, bool esGasNoble) 
            : base(nombre, simboloQuimico, numeroAtomico, anyoDescubrimeinto)
        {
            EsGasNoble = esGasNoble;   
        }
        //métodos de acceso y modificación
        public bool EsGasNoble { get => esGasNoble; set => esGasNoble = value; }
        
        //sobrecarga del método ToString para la escritura del fichero
        public override string ToString()
        {
            return "NoMetal;" + base.ToString() + ";" + (esGasNoble?"gas noble":"no gas noble");
        }
    }
}
