﻿/*En esta clase se crean los Elementos del tipo Metaloide
 Contiene atributos, método constructor, métodos de modificación y acceso a los atributos y
 la sobrecarga del método ToString utilizado en la escritura del fichero de texto*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TablaPeriodica
{
    class Metaloide:Elemento
    {
        //atributos
        private bool esMetalRefractario;
        private bool esMetalNoble;
        
        //método constructor
        public Metaloide(string nombre, string simboloQuimico, byte numeroAtomico, string anyoDescubrimeinto,
            bool esMetalRefractario, bool esMetalNoble) 
            : base (nombre, simboloQuimico, numeroAtomico, anyoDescubrimeinto)  
        {
            this.EsMetalRefractario = esMetalRefractario;
            this.EsMetalnomble = esMetalNoble;   
        }
        
        //métodos de acceso y modificación
        public bool EsMetalRefractario { get => EsMetalRefractario; set => EsMetalRefractario = value; }
        public bool EsMetalnomble { get => esMetalNoble; set => esMetalNoble = value; }
        
        //sobrecarga del método ToString para la escritura del fichero
        public override string ToString()
        {
            return "Metaloide;" + base.ToString() + ";" + (esMetalRefractario? "refractario":"no refractario") 
                + ";" + (esMetalNoble?"metal noble":"no metal noble");
        }
    }
}
