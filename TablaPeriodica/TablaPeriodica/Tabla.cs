﻿/*Esta clase contiene los métodos que permiten mostrar por pantalla las distintas tablas del programa*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TablaPeriodica
{
    class Tabla
    {
        public static void MostrarSigloActual(ArrayList listaElementos)
        {
            DateTime inicioSiglo = new DateTime(2001, 01, 01);
            DateTime actualidad = DateTime.Now;
            int inicioTabla = inicioSiglo.Year;
            int finTabla = actualidad.Year;

            Console.WriteLine("Siglo 21");
            Console.WriteLine();
            MostrarTabla(inicioTabla, finTabla, listaElementos);
            //Elimino estas líneas para que se muestr el menú junto al siglo actual en la primera pantalla
            //Console.WriteLine("Pulsa una tecla para continuar");
           // Console.ReadKey(true);
        }

        //Muestra por pantalla el siglo actual
        public static void MostrarSiglo(ArrayList listaElementos, byte siglo)
        {
            //variables propias del método
            DateTime inicioSiglo = new DateTime(siglo * 100 - 99, 01, 01);
            DateTime finSiglo = new DateTime(siglo * 100, 12, 31);
            int inicioTabla= inicioSiglo.Year;
            int finTabla = finSiglo.Year;
            
            Console.Clear();//limpieza de pantalla

            Console.WriteLine("Siglo {0}", siglo);
            Console.WriteLine();
            MostrarTabla(inicioTabla, finTabla, listaElementos);
            Console.WriteLine("Pulsa una tecla para continuar");
            Console.ReadKey(true);
        }

        //Musetra por pantalla una tabla con todos los siglos y el número de elementos descubiertos 
        public static void MostrarTodosSiglos(ArrayList listaElementos)
        {
            //variables propias del método
            byte siglo=1;
            byte contador=0;
            DateTime inicioSiglo = new DateTime(siglo * 100 - 99, 01, 01);
            DateTime finSiglo = new DateTime(siglo * 100, 12, 31);
            int inicioTabla = inicioSiglo.Year;
            int finTabla = finSiglo.Year;

            Console.Clear();//limpieza de pantalla

            //calculo elementos Edad Antigua
            foreach (Elemento elemento in listaElementos)
            {
                if (elemento.AnyoDescubrimeinto.Year < 0)
                    contador++;
            }
            Console.WriteLine("Edad Antigua: {0} elementos", contador);

            //cálculo de elementos del resto de siglos
            for (siglo = 1; siglo <= 21; siglo++)
            {
                foreach (Elemento elemento in listaElementos)
                {
                    if (elemento.AnyoDescubrimeinto.Year >= inicioSiglo.Year && 
                        elemento.AnyoDescubrimeinto.Year <= finSiglo.Year)
                        contador++;
                }
                Console.WriteLine("Siglo {0}  : {1} elementos", siglo, contador);
            }
            Console.WriteLine("Pulsa una tecla para continuar");
            Console.ReadKey(true);
        }

        //método auxiliar para mostrar la tabla desde un año hasta otro
        public static void MostrarTabla(int inicio, int fin, ArrayList listaElementos)
        {
            byte contadorMetal=0;
            byte contadorMetaloide = 0;
            byte contadorNoMetal = 0;
            for (int i = inicio; i <= fin; i=i+5)
            {
                for (int j = i; j < i+5; j++)
                {
                    if(j<=fin)
                    {
                        foreach (Elemento elemento in listaElementos)
                        {
                            if (elemento.AnyoDescubrimeinto.Year == j
                                && elemento.GetType().Equals(typeof(Metal)))
                                contadorMetal++;
                            if (elemento.AnyoDescubrimeinto.Year == j
                                && elemento.GetType().Equals(typeof(Metaloide)))
                                contadorMetaloide++;
                            if (elemento.AnyoDescubrimeinto.Year == j
                                && elemento.GetType().Equals(typeof(NoMetal)))
                                contadorNoMetal++;
                        }
                        //condiciones para los distintos formatos utilizados al mostrar los años
                        if (contadorMetal > 0 && contadorMetaloide == 0 && contadorNoMetal == 0)
                            Console.ForegroundColor = ConsoleColor.Red;
                        if (contadorMetal == 0 && contadorMetaloide > 0 && contadorNoMetal == 0)
                            Console.ForegroundColor = ConsoleColor.Blue;
                        if (contadorMetal == 0 && contadorMetaloide == 0 && contadorNoMetal > 0)
                            Console.ForegroundColor = ConsoleColor.Green;
                        if((contadorMetal > 0 && (contadorMetaloide >0 ||contadorNoMetal>0))|| 
                            (contadorMetaloide>0 && contadorNoMetal>0))
                        {
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.BackgroundColor = ConsoleColor.White;
                        }

                        Console.Write("{0} ", j);
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }

}
