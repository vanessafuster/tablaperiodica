﻿/*Clase Abstracta desde la que heredan los distintos tipos de elementos
 Contiene atributos, método constructor, métodos de modificación y acceso a los atributos y
 la sobrecarga del método ToString utilizado en la escritura del fichero de texto*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TablaPeriodica
{
    abstract class Elemento
    {
        //atributos
        protected string nombre;
        protected string simboloQuimico;
        protected byte numeroAtomico;
        protected DateTime anyoDescubrimeinto;
        protected bool elementoAntiguo;

        //método constructor
        public Elemento(string nombre, string simboloQuimico, byte numeroAtomico, string anyoDescubrimeinto)
        {
            this.Nombre = nombre;
            this.SimboloQuimico = simboloQuimico;
            this.NumeroAtomico = numeroAtomico;
            if(anyoDescubrimeinto.Equals("A.C."))
            {
                this.anyoDescubrimeinto = new DateTime(-1, 1, 1);
                elementoAntiguo = true;
            }
            else
            {
                this.anyoDescubrimeinto = new DateTime(Convert.ToInt32(anyoDescubrimeinto), 1, 1);
                elementoAntiguo = false;
            }        
            
        }
        //métodos de acceso y modificación de atributos
        protected string Nombre { get => nombre; set => nombre = value; }
        protected string SimboloQuimico { get => simboloQuimico; set => simboloQuimico = value; }
        protected byte NumeroAtomico { get => numeroAtomico; set => numeroAtomico = value; }
        public DateTime AnyoDescubrimeinto { get => anyoDescubrimeinto; set => anyoDescubrimeinto = value; }
        protected bool ElementoAntiguo { get => elementoAntiguo; set => elementoAntiguo = value; }

        //sobrecarga del método ToString para la escritura del fichero
        public override string ToString()
        {
            return nombre + ";" + simboloQuimico + ";" + numeroAtomico + ";" + 
                (elementoAntiguo ? "A.C." : anyoDescubrimeinto.Year.ToString());
        }
    }


}
