﻿/*Esta clase contiene los métods relativos al volcado de información desde archivo de texto a la colección
 * de elementos y viceversa*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

namespace TablaPeriodica
{
    class GestorElementos
    {
        private const string NonmbreFichero = "elementos.txt";
        /*lee el contenido del archivo “elementos.txt” (si existe), 
          crea un elemento del tipo correspondiente con cada línea que lea del fichero,
           y lo almacenará en una colección de elementos.
             Al final del proceso, devolvuelve esa colección como resultado*/
        public static ArrayList Cargar()
        {          
            //variables propias del método
            string linea;
            string [] lineaPartida;
            string archivo = @"..\..\..\elementos.txt";

            //se crea colección para almacenar los distintos elementos
            ArrayList listaElementos = new ArrayList();

            //inicio de la lectura del fichero "elementos.txt"
            if (File.Exists(archivo))
            { 
                try
                {
                    using (StreamReader fichero = new StreamReader(archivo))
                    {
                        do
                        {
                            linea = fichero.ReadLine();
                            if (linea != null)
                            {
                                lineaPartida = linea.Split(';');
                                if (lineaPartida[0].ToUpper().Equals(("Metal").ToUpper()))
                                {
                                    listaElementos.Add(new Metal(lineaPartida[1],//nomnre
                                            lineaPartida[2],//símbolo
                                            Convert.ToByte(lineaPartida[3]),//número atómico
                                            lineaPartida[4],//A.C. o año
                                            lineaPartida[5] == "liquido" ? true : false,//líquido o no líquido
                                            lineaPartida[6], //color
                                            lineaPartida[7] == "metal noble" ? true : false));
                                    // metal noble o no metal noble
                                    Console.WriteLine("Campos fichero leídos");
                                }
                                else if (lineaPartida[0].ToUpper().Equals(("Metaloide").ToUpper()))
                                {
                                    listaElementos.Add(new Metaloide(lineaPartida[1],
                                           lineaPartida[2],
                                           Convert.ToByte(lineaPartida[3]),
                                           lineaPartida[4],
                                           lineaPartida[5] == "refractario" ? true : false,
                                           lineaPartida[6] == "metal noble" ? true : false));
                                }
                                else if (lineaPartida[0].ToUpper().Equals(("NoMetal").ToUpper()))
                                {
                                    listaElementos.Add(new NoMetal(lineaPartida[1],
                                           lineaPartida[2],
                                           Convert.ToByte(lineaPartida[3]),
                                           lineaPartida[4],
                                           lineaPartida[5] == "gas noble" ? true : false));
                                }
                            }
                        } while (linea != null);
                    }
                }catch(Exception exp)
                {
                    Console.WriteLine("Ha habido un error: {0}", exp.Message);
                }
            }
            return listaElementos;
        }
        
        /*si no está vacía, almacena los elementos de la colección en el archivo elementos.txt*/
        public static void Guardar(ArrayList listaElementos)
        {
            using (StreamWriter ficheroSalida = new StreamWriter(@"..\..\..\elementos.txt"))
            {
                for (byte i = 0; i < listaElementos.Count; i++)
                {
                    ficheroSalida.WriteLine(listaElementos[i].ToString());
                }
            }
        }
    }
}
