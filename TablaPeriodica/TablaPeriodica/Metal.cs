﻿/*En esta clase se crean los Elementos del tipo Metal
 Contiene atributos, método constructor, métodos de modificación y acceso a los atributos y
 la sobrecarga del método ToString utilizado en la escritura del fichero de texto*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TablaPeriodica
{
    class Metal: Elemento
    {
        //atributos
        private bool esLiquido;
        private string color;
        private bool esMetalNoble;

        //método constructor
        public Metal(string nombre, string simboloQuimico, byte numeroAtomico, string anyoDescubrimeinto,
            bool esLiquido, string color, bool esMetalNoble) : base (nombre, simboloQuimico, 
                numeroAtomico, anyoDescubrimeinto)
        { 
            this.EsLiquido = esLiquido;
            this.Color = color;
            this.EsMetalNoble = esMetalNoble;
        }
        //métodos de acceso y modificación
        public bool EsLiquido { get => esLiquido; set => esLiquido = value; }
        public string Color { get => color; set => color = value; }
        public bool EsMetalNoble { get => esMetalNoble; set => esMetalNoble = value; }

        //sobrecarga del método ToString para la escritura del fichero
        public override string ToString()
        {
            return "Metal;"+ base.ToString() + ";" +( esLiquido? "liquido":"no liquido") + ";" + color + ";" 
                + (esMetalNoble? "metal noble":"no metal noble");
        }
    }
}
